import kolor.*
import java.lang.Exception
import java.util.*

val scanner = Scanner(System.`in`)
var palabras = arrayListOf("boyas", "alijo", "mundo", "acero", "abeto", "arcos", "agudo", "agrio", "jueza", "feliz", "veloz", "cajón", "marzo", "pinza", "zebra", "afine", "altos", "ambos", "aptos", "arpón", "acido")
var keyword = palabras.random()
var palabra = ""
var intents = 0
/** ALMACEN DE LAS PALABRAS */
var respondidas = mutableListOf<String>()
fun main() {
    var decision: String
    do{
        println("Bienvenido a Wordle!")
        println("Introduce una palabra de 5 letras para poder empezar: ")
        println(keyword)
        while (intents != 6 && palabra != keyword) {

            var palabra = checkword()

            /** COMPROBACIÓN CARACTER POR CARACTER */
            checkchar()
            if (palabra == keyword) {
                registros()
                println("Has guanyat!")
                println("Vols jugar un altre partida? SI - NO")

            }
            registros()
            intents++
        }
        if (intents == 6){
            println("Has perdut! La paraula era: $keyword")
            println("Vols jugar un altre partida? SI - NO")

        }
        decision= scanner.next()

    }while(reset(decision))

}

/** MOSTRAR EL HISTORIAL DE UNA LISTA MUTABLE LLAMADA RESPONDIDAS*/
fun registros(){
    respondidas.forEachIndexed { index, valor ->
        if (index % 5 == 0) println() /**Para mostrar línea por línea*/
        print(valor)
    }
    println()
}
/**REVISA LA LONGITUD DE LA PALABRA*/
fun checkword(): String{
    var distancia = 0
    while (distancia != 5) {
        palabra = scanner.next().lowercase()
        distancia = palabra.length
        if (distancia != 5) println("Ha de ser una paraula amb 5 lletres")
    }
    return palabra
}

/**REVISA LAS PALABRAS CARÁCTER POR CARÁCTER Y EN TODAS LAS CONDICIONES SE AÑADEN EN LA LISTA MUTABLE RESPONDIDAS*/
fun checkchar() {
    var repetidas = keyword.split("").toMutableList()
    repetidas.removeAt(0)
    repetidas.removeAt(4)
    var resultado = mutableListOf("X","X","X","X","X")
    palabra.forEachIndexed { index, c ->
        if (c == keyword[index]) {
            resultado.set(index, " $c".greenBackground().lightWhite())
            repetidas.remove(c.toString())
        }
    }
    palabra.forEachIndexed { index, c ->
        if (c == keyword[index]) {
            resultado.set(index, " $c".greenBackground().lightWhite())
        } else if (c.toString() in repetidas) {
            resultado.set(index, " $c".yellowBackground().lightWhite())
            repetidas.remove(c.toString())
        } else{
            resultado.set(index, " $c".blackBackground().lightWhite())
        }
    }
    respondidas += resultado
}


/**REINICIA LAS ESTADISTICAS DE INTENTOS, LA LISTA MUTABLE, VUELVE A GENERAR OTRA PALABRA NUEVA Y REINICIA EL MAIN*/
fun reset(des: String): Boolean{
    if (des.lowercase() == "si"){
        intents = 0
        respondidas = mutableListOf<String>()
        keyword = palabras.random()
        return true
    }
    else return false
}









