package kolor

object Kolor {
    internal const val ESCAPE = '\u001B'
    internal const val RESET = "$ESCAPE[0m"

    fun foreground(string: String, color: Color) = Kolor.color(string, color.foreground)

    fun background(string: String, color: Color) = Kolor.color(string, color.background)

    private fun color(string: String, ansiString: String) = "$ansiString$string$RESET"
}