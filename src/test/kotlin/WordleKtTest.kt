import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class WordleKtTest{
    @Test
    fun checkgame(){
        val decision = "si"
        val expected = true
        assertEquals(expected, reset(decision))
    }
    @Test
    fun checkgameno(){
        val decision = "no"
        val expected = false
        assertEquals(expected, reset(decision))
    }
    @Test
    fun checkgameempty(){
        val decision = ""
        val expected = false
        assertEquals(expected, reset(decision))
    }

}